package com.nexperia.ditaotwebclient;

import com.nexperia.ditaotwebclient.services.DitaCommandRunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Controller
public class DitaConvertToHtmlController {

    @Autowired
    DitaCommandRunService ditaCommandRunService;

    @GetMapping("/")
    public String listUploadedFiles(Model model) throws IOException {
        return "uploadForm";
    }

    @PostMapping("/")
    @ResponseBody
    public ResponseEntity<Resource> handleDitaContent(@RequestParam("file") MultipartFile ditaFiles) throws IOException {
        File file = new File((new Date()).getTime() + "_source.zip");
        FileCopyUtils.copy(ditaFiles.getInputStream(), new FileOutputStream(file));
        File zip = null;
        try {
            zip = ditaCommandRunService.getZippedResult(UUID.randomUUID().toString(), file);
            InputStreamResource resource = new InputStreamResource(new FileInputStream(zip));
//        InputStreamResource resource = new InputStreamResource(ditaFile.getInputStream());
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=\"" + zip.getName() + "\"").body(resource);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }

}
