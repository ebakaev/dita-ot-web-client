package com.nexperia.ditaotwebclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DitaOtWebClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(DitaOtWebClientApplication.class, args);
	}

}
