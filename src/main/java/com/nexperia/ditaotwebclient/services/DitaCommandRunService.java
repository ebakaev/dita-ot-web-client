package com.nexperia.ditaotwebclient.services;

import org.springframework.stereotype.Service;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@Service
public class DitaCommandRunService {

    void convertDitamapToHTML(File ditamap) throws IOException, InterruptedException {
        String source = ditamap.getAbsolutePath().replace("\\", "/");

        // Build process to execute convert
        ProcessBuilder pb = new ProcessBuilder(
                "dita",
                "-i",
                source,
                "-o",
                ditamap.getParent(),
                "-f",
//                "html5");
                "xhtml");
        pb.redirectErrorStream(true);
        System.out.println("Command executing : " + pb.command());
        System.out.println("source : " + source);
        Process p = pb.start();
        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line = null;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        // any error???
        int exitVal = p.waitFor();
        System.out.println("Process exitValue: " + exitVal);
        // success
        System.out.println("convert command executed successfully.");
    }

    public File getZippedResult(String id, File sourceDita) throws IOException, InterruptedException {
        prepareHtmlRenditions(id, sourceDita);

        File zipFile = new File("htmlRendition_" + id + ".zip");
        FileOutputStream fos = new FileOutputStream(zipFile);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        File fileToZip = new File(id);

        zipFile(fileToZip, fileToZip.getName(), zipOut);
        zipOut.close();
        fos.close();

        return zipFile;
    }

    private static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        if (fileToZip.isHidden()) {
            return;
        }
        if (fileToZip.isDirectory()) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            File[] children = fileToZip.listFiles();
            for (File childFile : children) {
                zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
            return;
        }
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
    }

    private void prepareHtmlRenditions(String id, File sourceDitaArchive) throws IOException, InterruptedException {
        File resultFolder = new File(id);
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(sourceDitaArchive));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = newFile(resultFolder, zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs()) {
                    throw new IOException("Failed to create directory " + newFile);
                }
            } else {
                // fix for Windows-created archives
                File parent = newFile.getParentFile();
                if (!parent.isDirectory() && !parent.mkdirs()) {
                    throw new IOException("Failed to create directory " + parent);
                }

                // write file content
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();

        System.out.println("Searching dita map...");
        File[] matchingFiles = resultFolder.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                System.out.println("found: " + name);
                return name.endsWith(".ditamap");
            }
        });
        if (matchingFiles.length == 1) {
            System.out.println("start converting " + matchingFiles[0].getAbsolutePath());
            convertDitamapToHTML(matchingFiles[0]);
        }
    }

    private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }
}